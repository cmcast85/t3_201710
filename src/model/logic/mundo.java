package model.logic;


import model.data_structures.Stack;

public class mundo {

	
	private Stack<Character> pila;
	
	public boolean expresionBienFormada (String expresion){
		char[] ch = expresion.toCharArray();
		int contador1 = 0;
		int contador2 = 0;
		boolean pasa = true;
		for(int i = 0; i <ch.length && pasa;i++){
			pasa = pila.push((ch[i]));
			switch(ch[i]){
			case '[' : contador1++;
			case ']' : contador1--;
			case '(' : contador2++;
			case ')' : contador2--;
			}
		}
		if(contador1 == 0 && contador2 ==0){
			pasa = true;
		}
		
		return pasa;
	}
	
	public Stack<Character> ordenar(){
		Stack<Character> temporal = new Stack<Character>();
		Stack<Character> ordenada = new Stack<Character>();
		
		for(Character ch : pila ){
			temporal.push(pila.pop());
		}
		
		for(int i = 0;i<temporal.size();i++ ){
			char temp = temporal.pop();
			switch(temp){
			case '1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'|'9'|'0' : ordenada.push(temp);
			}
		}
		for(int i = 0;i<temporal.size();i++ ){
			char temp = temporal.pop();
			switch(temp){
			case '+'|'*'|'-'|'/' : ordenada.push(temp);
			}
		}
		for(int i = 0;i<temporal.size();i++ ){
			char temp = temporal.pop();
			switch(temp){
			case '['|']'|'('|')' : ordenada.push(temp);
			}
		}
		
		return ordenada;
	}

}
