package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private Nodo<T> cabeza;
	private Nodo<T> posicionActual;
	private int tama�o;
	
	public ListaEncadenada (T primerElemento){
		cabeza = new Nodo<T>(primerElemento);
		posicionActual = cabeza;
		tama�o = 1;
	}
	
	public ListaEncadenada (){
		tama�o = 0;
		cabeza = null;
		posicionActual= null;
	}
	
	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			Nodo<T> actual = null;
			@Override
			public boolean hasNext() {
				if (actual == null || actual.darSiguiente() != null) return true;
				else return false;
			}

			@Override
			public T next() {
				if (actual == null) actual = cabeza;
				else actual = actual.darSiguiente();
				return actual.darObjeto();
			}
		};
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if(cabeza==null){
			cabeza = new Nodo<T>(elem);
			posicionActual = cabeza;
		}
		else {
			Nodo<T> actual = cabeza;
			
			while (actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
			}
			Nodo<T> siguiente = new Nodo<T>(elem);
			actual.cambiarSiguiente(siguiente);
		}
		tama�o++;
	}

	@Override
	public T darElemento(int pos) {
		Nodo<T> actual = cabeza;
		for (int i = 0; i < pos-1; i++) {
			if(actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
			}
			else return null;
		}
		return actual.darObjeto();
	}


	@Override
	public int darNumeroElementos() {
		return tama�o;
	}

	public T darElementoPosicionActual() {
		return posicionActual.darObjeto();
	}

	public boolean avanzarSiguientePosicion() {
		if (posicionActual.darSiguiente() != null) {
			posicionActual = posicionActual.darSiguiente();
			return true;
		}
		else return false;
	}

	public boolean retrocederPosicionAnterior() {
		Nodo<T> anterior = cabeza;
		if (posicionActual.equals(cabeza)) {
			return false;
		}
		while (!anterior.darSiguiente().equals(posicionActual)) {
			anterior = anterior.darSiguiente();
		}
		posicionActual = anterior;
		return true;
	}
	
	public boolean isEmpty()
	{
		return tama�o==0;
	}

	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

}
