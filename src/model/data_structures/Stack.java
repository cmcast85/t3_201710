package model.data_structures;

import java.util.Iterator;

/**
 * Clase que representa una pila genérica.
 * @author jlake
 */
public class Stack<T> implements Iterable<T> {
	
	/**
	 * Atributo que representa el primer elemento del stack.
	 */
	private Nodo<T> primero;
	
	/**
     * Crea un Stack vacio.
     */
    public Stack() 
    {
    	primero = null;
    }

    /**
     * Pone un item en la parte superior del stack.
     * @param   item  El item que se desea ubicar arriba del stack.
     * @return  True si se agrega el elemento, false de lo contrario.
     */
    public boolean push(T item) {
    	boolean put = false;
        Nodo<T> nuevo = new Nodo<T>(item);
        nuevo.cambiarSiguiente(primero);
        primero = nuevo;
        put = true;
        return put;
    }

    /**
     * Remueve el objeto al tope del stack y devuelve ese objeto.
     * @return     El objeto ubicado de primero ( el último en entrar)  en el Stack. null si el stack estaba vacío.
     */
    public synchronized T pop() {
        T obj;
        int size = size();
        if(size == 0)
        	return null;
        obj = peek();
        primero = primero.darSiguiente();
        return obj;
    }

    /**
     * Método que devuelve el tamaño del Stack.
     * @return el tamaño del stack en int. 0 si está vacío.
     */
    public int size() {
    	int size = 0;
        Nodo<T> nodo = primero;
        
        while( nodo != null)
        {
            size++;
            nodo = nodo.darSiguiente( );
        }
        return size;
	}

	/**
     * Permite mirar el objeto que se encuentra de último en el stack sin necesidad de removerlo.
     * @return     El objeto ubicado de primero ( el último en entrar) en el Stack. null si está vacío el Stack.
     */
    public synchronized T peek() {
    	if(isEmpty())
    		return null;
        return primero.darObjeto();
    }

    /**
     * Revisa si el stack es vacío.
     * @return  true si el stack es no tiene elementos, false de lo contrario.
     */
    public boolean isEmpty() {
        return (primero == null);
    }

    /**
     * Devuelve la distancia desde el primer objeto del stack ( que se considera que está a una distancia 1) 
     * hasta el objeto que se desea buscar, que llega por parámetro.
     * @param   obj El objeto a buscar
     * @return  La posición (o distancia) en el Stack donde está el objeto buscado, si se encuentra.
     * 			En caso de no encontrarlo, retorna -1.
     */
    public synchronized int search(T obj) {
    	int index = -1;
        boolean found = false;
        Nodo<T> ele =primero;
        for( int i = 0; !found; i++)
        {
            if(ele == null)
                break;
            if(ele.darObjeto( ).equals( obj )){
            	index = i;
            	found = true;
            }
            ele = ele.darSiguiente( );
        }
        return index;
    }
    
    /**
     * Devuelve el elemento de la posici�n dada
     * @param pos la posici�n  buscada
     * @return el elemento en la posici�n dada 
     * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
     */
    public T get( int pos )
    {
        if(pos<0)
            throw new ArrayIndexOutOfBoundsException( );
        boolean found = false;
        int count = 0;
        Nodo<T> ele = primero;
        if(primero == null)
            return null;
        while (!found)
        {
            if(ele == null )
                throw new ArrayIndexOutOfBoundsException( );
            if(count == pos)
                 break;
            count++;
            ele = ele.darSiguiente( );
        }
        return ele.darObjeto( );
    }
    
    /**
     * Devuelve un iterador sobre la lista
     * El iterador empieza en el primer elemento
     * @return un nuevo iterador sobre la lista
     */
    public Iterator<T> iterator( )
    {
        return new IteradorSencillo<T>( primero );
    }
    
}